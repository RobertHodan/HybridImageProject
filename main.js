function main() {
    let manager = new CompositionManager({
        containerId: 'canvas-container',
    });

    // Window settings
    // Changes the size of the window
    window.resizeTo(1340, 920);

    // Listens for changes in the control-panel element
    attachControlPanelListeners(manager);

    // Load images into the manager based on the path name

    // Brad / Chris
    manager.loadImage({
        id: 'h1Brad',
        path: 'images/h1_brad-01.png',
        thumnail: 'images/h1_brad-02.png',
    });
    manager.loadImage({
        id: 'h1Chris',
        path: 'images/h1_chris-01.png',
        thumnail: 'images/h1_chris-02.png',
    });

    // Hotdog / Weiner
    manager.loadImage({
        id: 'h2Hotdog',
        path: 'images/h2_hotdog-01.png',
        thumnail: 'images/h2_hotdog-02.png',
    });
    manager.loadImage({
        id: 'h2Weiner',
        path: 'images/h2_weiner-01.png',
        thumnail: 'images/h2_weiner-02.png',
    });

    // Getaway / Stayaway
    manager.loadImage({
        id: 'h3Getaway',
        path: 'images/h3_getaway-01.png',
        thumnail: 'images/h3_getaway-02.png',
    });
    manager.loadImage({
        id: 'h3Stayaway',
        path: 'images/h3_stayaway-01.png',
        thumnail: 'images/h3_stayaway-02.png',
    });

    // Pi / Pie
    manager.loadImage({
        id: 'h4Pi',
        path: 'images/h4_pi-01.png',
        thumnail: 'images/h4_pi-02.png',
    });
    manager.loadImage({
        id: 'h4Pie',
        path: 'images/h4_pie-01.png',
        thumnail: 'images/h4_pie-02.png',
    });

    // Toes / Towtruck
    manager.loadImage({
        id: 'h5Toes',
        path: 'images/h5_toes-01.png',
        thumnail: 'images/h5_toes-02.png',
    });
    manager.loadImage({
        id: 'h5Towtruck',
        path: 'images/h5_towtruck-01.png',
        thumnail: 'images/h5_towtruck-02.png',
    });

    // Create compositions
    manager.composite({
        task: 'hybrid',
        imageIds: ['h1Chris', 'h1Brad'],
        radius: [6, 3],
        opacity: [0.6, 0.5],
        outputId: 'hybrid1',
        scale: 0.92,
    });

    manager.composite({
        task: 'hybrid',
        imageIds: ['h2Hotdog', 'h2Weiner'],
        radius: [20, 4],
        opacity: [0.6, 0.5],
        outputId: 'hybrid2',
        scale: 0.92,
    });

    manager.composite({
        task: 'hybrid',
        imageIds: ['h3Getaway', 'h3Stayaway'],
        radius: [14, 2],
        opacity: [0.65, 0.65],
        outputId: 'hybrid3',
        scale: 0.92,
    });

    manager.composite({
        task: 'hybrid',
        imageIds: ['h4Pie', 'h4Pi'],
        radius: [20, 1],
        opacity: [0.5, 0.7],
        outputId: 'hybrid4',
        scale: 0.92,
    });

    manager.composite({
        task: 'hybrid',
        imageIds: ['h5Towtruck', 'h5Toes'],
        radius: [20, 4],
        opacity: [0.6, 0.55],
        outputId: 'hybrid5',
        scale: 0.92,
    });

    manager.setHybridImageList([
        'hybrid1', 'hybrid2', 'hybrid3', 'hybrid4', 'hybrid5'
    ]);

    let wrapper = document.getElementById('wrapper');

    // Apply image to canvas
    manager.newTask({
        task: 'applyToCanvas',
        imageId: 'hybrid1',
        canvasId: 'hybrid-image',
    });
}

function attachControlPanelListeners(manager) {
    // If we have time, maybe improve this
    let scaleSlider = document.getElementById("image-scale-slider");
    scaleSlider.onchange = function(event) {
        let newValue = parseInt(event.target.value) / 100;
        let containerId = manager.getCanvasContainerId();
        let id = manager.getActiveHybrid();
        let settings = manager.getHybridSettings(id);
        settings.scale = newValue;
        manager.eventCalled({
            eventName: 'scaleImage',
            containerId: containerId,
            scale: newValue,
        });
    }
    scaleSlider.oninput = function(event) {
        manager.updateSliderNumbers();
    }

    // Blur for low spatial frequency
    let lsfBlurSlider = document.getElementById("lsf-blur-slider");
    lsfBlurSlider.onchange = function(event) {
        let newValue = parseInt(event.target.value);
        let id = manager.getActiveHybrid();
        let settings = manager.getHybridSettings(id);
        settings.radius[0] = newValue;

        manager.eventCalled({eventName: 'settingsChanged'});
    }
    lsfBlurSlider.oninput = function(event) {
        manager.updateSliderNumbers();
    }

    // Blur for high spatial frequency
    let hsfBlurSlider = document.getElementById("hsf-blur-slider");
    hsfBlurSlider.onchange = function(event) {
        let newValue = parseInt(event.target.value);
        let id = manager.getActiveHybrid();
        let settings = manager.getHybridSettings(id);
        settings.radius[1] = newValue;

        manager.eventCalled({eventName: 'settingsChanged'});
    }
    hsfBlurSlider.oninput = function(event) {
        manager.updateSliderNumbers();
    }

    // Opacity for low spatial frequency
    let lsfOpacitySlider = document.getElementById("lsf-opacity-slider");
    lsfOpacitySlider.onchange = function(event) {
        let newValue = parseInt(event.target.value)/100;
        let id = manager.getActiveHybrid();
        let settings = manager.getHybridSettings(id);
        settings.opacity[0] = newValue;

        manager.eventCalled({eventName: 'settingsChanged'});
    }
    lsfOpacitySlider.oninput = function(event) {
        manager.updateSliderNumbers();
    }

    // Opacity for low spatial frequency
    let hsfOpacitySlider = document.getElementById("hsf-opacity-slider");
    hsfOpacitySlider.onchange = function(event) {
        let newValue = parseInt(event.target.value)/100;
        let id = manager.getActiveHybrid();
        let settings = manager.getHybridSettings(id);
        settings.opacity[1] = newValue;

        manager.eventCalled({eventName: 'settingsChanged'});
    }
    hsfOpacitySlider.oninput = function(event) {
        manager.updateSliderNumbers();
    }

    // Hook up buttons
    // Left arrow
    let button = document.getElementById('left-icon');
    button.onclick = function(event) {
        manager.previousImage();
    }
    // Right arrow
    button = document.getElementById('right-icon');
    button.onclick = function(event) {
        manager.nextImage();
    }

    // Reset button
    button = document.getElementById('reset');
    button.onclick = function(event) {
        manager.resetActiveHybridImage();
    }

    // Flip button
    button = document.getElementById('flip-button');
    button.onclick = function(event) {
        manager.flipFrequencies();
    }

    document.onscroll = function(event) {
        manager.updateSliderNumbers();
    }
}

function infopanelOn() {
    document.getElementById("infopanel").style.display = "block";
}

function infopanelOff() {
    document.getElementById("infopanel").style.display = "none";
}