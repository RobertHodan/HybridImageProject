class GImage { // Graphic Image
    constructor(options = {}) {
        this.canvas = document.createElement('canvas');
        this.id = options.id;
        this.canvas.setAttribute('id', options.id);
        this.context = this.canvas.getContext('2d');
        this.width = 50;
        this.height = 100;
        this.paths = [options.path] || null;
        this.thumbnails = options.thumbnails || [options.thumnail];

        if (options.copy) {
            this._createCopy(options);
        } else {
            this._createNew(options);
        }
        this.imageData = this.getImageData();
        this.savedImageData = [];
    }

    getCanvas() {
        return this.canvas;
    }

    getContext() {
        return this.context;
    }

    getId() {
        return this.id;
    }

    getImageData() {
        return this.context.getImageData(0, 0, this.width, this.height);
    }

    getImagePath() {
        return this.path;
    }

    getImageThumbnail(num = 0) {
        return this.thumbnails[num];
    }

    setImageThumbnails(thumbnails) {
        this.thumbnails = thumbnails;
    }

    // Can be a performance concern
    // Use mainly for debugging
    getRGB(x, y) {
        let imageData = this.imageData;
        let rgb = {};
        rgb['red'] = imageData.data[((y * (imageData.width * 4)) + (x * 4))];
        rgb['green'] = imageData.data[((y * (imageData.width * 4)) + (x * 4)) + 1];
        rgb['blue'] = imageData.data[((y * (imageData.width * 4)) + (x * 4)) + 2];
        rgb['alpha'] = imageData.data[((y * (imageData.width * 4)) + (x * 4)) + 3];

        return rgb;
    }

    getWidth() {
        return this.imageData.width;
    }

    getHeight() {
        return this.imageData.height;
    }

    pushImageData(imageData = this.imageData) {
        this.savedImageData.push(imageData);
    }

    // Modifies the image data variable
    // Does not modify the image
    setRGB(rgb, x, y) {
        let imageData = this.imageData;
        imageData.data[((y * (imageData.width * 4)) + (x * 4))] = rgb.red;
        imageData.data[((y * (imageData.width * 4)) + (x * 4)) + 1] = rgb.green;
        imageData.data[((y * (imageData.width * 4)) + (x * 4)) + 2] = rgb.blue;
        imageData.data[((y * (imageData.width * 4)) + (x * 4)) + 3] = rgb.alpha;
        this.imageData = imageData;
    }

    // Resets the image data to a saved one
    // Currently just the first one - we don't need multiple states
    reset() {
        let savedImageData = this.savedImageData[0];
        this.applyImageData(savedImageData);
    }

    // Resets the image data
    resetChanges() {
        this.imageData = this.getImageData();
    }

    // Apply the image data to the image
    applyImageData(imageData = this.imageData) {
        this.context.putImageData(imageData, 0, 0);
        this.imageData = imageData;
    }

    _createNew(options) {
        // If it's null, we know we should fall back to default css
        this.scale = options.scale || null;
        let img = options.img;
        this.img = img;

        this.canvas.setAttribute('width', img.width);
        this.canvas.setAttribute('height', img.height);
        this.context.drawImage(img, 0, 0, img.width, img.height);
        this.width = img.width;
        this.height = img.height;
    }

    // Create a copy of a given image
    _createCopy(options) {
        let copy = options.copy;

        this.scale = options.scale || copy.scale;
        this.width = copy.width;
        this.height = copy.height;

        this.canvas.setAttribute('width', this.width);
        this.canvas.setAttribute('height', this.height);
        this.context.drawImage(copy.canvas, 0, 0);
    }
}