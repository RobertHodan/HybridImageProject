class ImageUtility {
    constructor(options) {
        this._canvas = document.createElement('canvas');
        this._canvasContext = this._canvas.getContext('2d');
    }

    createImage(options = {}) {
        let img = new GImage(options);

        return img;
    }

    // params: width, height
    createImageData(options = {}) {
        let imageData;

        if (options.copy) {
            imageData = this._canvasContext.createImageData(options.copy.width, options.copy.height);
            imageData.data.set(options.copy.data);
        } else {
            imageData = this._canvasContext.createImageData(options.width, options.height);
        }

        return imageData;
    }
}