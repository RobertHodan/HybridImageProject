class ImageManager extends ImageUtility {
    constructor(options) {
        super();
        this.images = {};

        // Images that are waiting to be loaded
        this.pendingImages = {};
        this.imageLoadedCb = options.imageLoadedCb || function() {};
    }

    addImage(options) {
        let image = this.images[options.id];
        // If the output image exists, then update it. Otherwise, add it to the image manager
        if (image) {
            image.applyImageData(options.gImage.getImageData());
        } else {
            this.images[options.id] = options.gImage;
            this.imageLoadedCb(options.id);
        }
    }

    getImage(id) {
        return this.images[id] || false;
    }

    getImages(ids) {
        let images = [];
        for (let i = 0; i < ids.length; i++) {
            let image = this.images[ids[i]];
            if (image) {
                images.push(image);
            }
        }
        return images;
    }

    createImage(options = {}) {
        if (this.images[options.id]) {
            console.error(`Image with "${options.name}" id already exists`)
        }

        let img = super.createImage(options);
        this.images[options.id] = img;

        return img;
    }

    isImageLoaded(id) {
        return !!this.images[id];
    }

    // Adds the image to the image manager once it's loaded
    loadImage(options = {}) {
        if (this.images[options.id]) {
            console.error(`Image with "${options.name}" id already exists`)
        }

        let imageStorage = document.getElementById('image-storage');

        let img;
        // Looks for images in an image-storage container
        // If it's not present, attempts to load the image with a given path
        if (imageStorage) {
            img = this._findImage(options.path, imageStorage.getElementsByTagName('img'));
        } else {
            img = new Image();
            img.src = options.path;
        }

        options.img = img;
        delete options.path;

        // Check if the image has loaded
        if (!img.complete && img.naturalHeight === 0) {
            this.pendingImages[options.id] = options;

            img.onload = () => {
                let opts = this.pendingImages[options.id];
                delete this.pendingImages[opts.id];

                // Create image, and add it to our image hash
                this._addImage(opts);
            }
        } else {
            this._addImage(options);
        }
    }

    // Create an image, and add it to the images hash
    _addImage(options) {
        let img = this.createImage(options);
        this.images[options.id] = img;
        // Execute callback with the image id
        this.imageLoadedCb(options.id);
    }

    // Find images in a specified element
    _findImage(src, images) {
        for (let i = 0; i < images.length; i++) {
            let image = images[i];
            let fullPath = image.src;
            if (fullPath.includes(src)) {
                return image;
            }
        }

        return false;
    }
}