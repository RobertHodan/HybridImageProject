// Main class for compositions
// Handles tasks
class CompositionManager {
    constructor(options) {
        // For debugging
        window.manager = this;

        // Settings used to generate the hybrid image
        // Contains default values, and current values
        this.hybridSettings = {};
        // The hybrid image that is currently being displayed
        this.activeHybrid;
        // List of hybrid images
        this.hybridList = options.hybridList || [];

        this.pendingTasks = [];
        this.imageManager = new ImageManager({
            imageLoadedCb: this._dependencyLoaded.bind(this),
        });
        this.imageManipulator = new ImageManipulator();
        this.docManager = new DocumentManager({
            containerId: options.containerId,
        });

        this.totalTasksCompleted = 0;
    }

    getImage(id) {
        return this.imageManager.getImage(id);
    }

    setActiveHybrid(id) {
        this.activeHybrid = id;
    }

    setScaleTo(value, containerId) {
        this.docManager.setScaleTo(value, containerId);
    }

    setDefaultHybridSettings(id, options) {
        this.setHybridSettings(id, options, true);
    }

    setHybridImageList(list = []) {
        this.hybridList = list;
        this.docManager.createDots(list.length);
    }

    setHybridSettings(id, options, isDefault = false) {
        let settings = this.hybridSettings[id];
        if (!settings)  settings = this.hybridSettings[id] = {};
        let copy = JSON.parse(JSON.stringify(options)); // Make a copy of it

        // This information wasn't a part of the original options, so remove it.
        if (copy.taskNum || typeof(copy.taskNum) === 'number') delete copy.taskNum;

        if (isDefault) {
            settings.default = copy;
        } else {
            settings.current = copy;
        }
        
    }

    getActiveHybrid() {
        return this.activeHybrid;
    }

    getCanvasContainerId() {
        return this.docManager.getCanvasContainerId();
    }

    getDefaultHybridSettings(id) {
        return this.hybridSettings[id].default;
    }

    getHybridImageList() {
        return this.hybridList;
    }

    getHybridSettings(id) {
        return this.hybridSettings[id].current;
    }

    // Merely create a new task
    applyToCanvas(imageId) {
        this.newTask({
            task: 'applyToCanvas',
            imageId: imageId,
            canvasId: 'hybrid-image',
        });
    }

    append(options) {
        this.newTask({
            task: 'append',
            title: options.title,
            imageId: options.imageId,
            target: options.target,
        });
    }

    composite(options) {
        this.newTask(options);
    }

    eventCalled(options) {
        let sourceImage, targetImage, result;
        switch(options.eventName) {
            case 'scaleImage':
                this.setScaleTo(options.scale, options.containerId);
                break;
            case 'settingsChanged':
                this._settingsChanged();
                break;
            case 'nextImage':
                this.nextImage();
                break;
            case 'previousImage':
                this.previousImage();
                break;
        }
    }

    flipFrequencies() {
        let id = this.getActiveHybrid();
        let settings = this.getHybridSettings(id);
        let lsf = settings.imageIds[1]; // lsf = hsf
        let hsf = settings.imageIds[0]; // hsf = lsf
        let image = this.getImage(id);

        settings.imageIds = [lsf, hsf];
        manager.eventCalled({eventName: 'settingsChanged'});
    }

    loadImage(options) {
        this.imageManager.loadImage(options);
    }

    // Handles the creation of pending tasks
    newTask(options) {
        if (!options.task) {
            console.error("No task name given.");
        }

        options.taskNum = this.totalTasksCompleted;
        this.totalTasksCompleted++;
        let dependencies = this._getDependencies(options);

        options.pendingDependencies = {};
        // Check if the dependencies are loaded, if not,
        // then keep track of the pending dependencies
        for (let i = 0; i < dependencies.length; i++) {
            let isPending = !this.imageManager.isImageLoaded(dependencies[i]);

            if (isPending) {
                options.pendingDependencies[dependencies[i]] = true;
            }
        }

        // If there are no dependencies, then begin the task
        // Otherwise, postpone the task until the the images are loaded
        let length = Object.keys(options.pendingDependencies).length;
        if (length > 0) {
            this.pendingTasks.push(options);
        } else {
            this._beginTask(options);
        }
    }

    nextImage() {
        let activeImage = this.getActiveHybrid();
        let imageList = this.getHybridImageList();
        let index = imageList.indexOf(activeImage);
        let nextIndex = index+1 >= imageList.length ? 0 : index+1;

        this.setImage(imageList[nextIndex]);
    }

    previousImage() {
        let activeImage = this.getActiveHybrid();
        let imageList = this.getHybridImageList();
        let index = imageList.indexOf(activeImage);
        let previousIndex = index-1 < 0 ? imageList.length-1 : index-1;

        this.setImage(imageList[previousIndex]);
    }

    resetActiveHybridImage() {
        let activeImage = this.getActiveHybrid();
        let defaultSettings = this.getDefaultHybridSettings(activeImage);
        this.setHybridSettings(activeImage, defaultSettings);
        this._settingsChanged();
    }

    updateSliderNumbers() {
        this.docManager.updateSliderNumbers();
    }

    setImage(imageId) {
        this.setActiveHybrid(imageId);
        this.applyToCanvas(imageId);
    }

    _executeApplyToCanvas(options) {
        let gImage = this.imageManager.getImage(options.imageId);
        let settings = this.getHybridSettings(options.imageId);
        this.docManager.applyToCanvas(options.canvasId, gImage, settings);

        let list = this.getHybridImageList();
        let index = list.indexOf(options.imageId);
        this.docManager.setActiveDot(index);
        this.activeHybrid = options.imageId;
    }

    // Executes specified tasks
    _beginTask(options) {
        let gImage, sourceImage, targetImage, result, images, mattes, settings;
        switch(options.task) {
            case 'append':
                // Get the image from the image manager
                gImage = this.imageManager.getImage(options.imageId);

                this.docManager.append({
                    title: options.title,
                    gImage: gImage,
                    target: options.target,
                    taskNum: options.taskNum,
                });
                break;

            case 'applyToCanvas':
                this._executeApplyToCanvas(options);
                break;

            case 'blur':
                sourceImage = this.imageManager.getImage(options.imageId);
                targetImage = this.imageManager.getImage(options.outputId);

                result = this.imageManipulator.blur({
                    gImage: sourceImage,
                    outputId: options.outputId,
                    scale: options.scale,
                    radius: options.radius,
                });

                this.imageManager.addImage({
                    gImage: result,
                    id: options.outputId,
                });
                break;

            case 'highPass':
                sourceImage = this.imageManager.getImage(options.imageId);
                targetImage = this.imageManager.getImage(options.outputId);

                result = this.imageManipulator.highPass({
                    gImage: sourceImage,
                    outputId: options.outputId,
                    scale: options.scale,
                    radius: options.radius,
                });

                this.imageManager.addImage({
                    gImage: result,
                    id: options.outputId,
                });
                break;

            // Image 1 is used for the low spatial frequency
            // Image 2 is used as the high spatial frequency
            // Parameters:
                // imageIds: array( id of image 1, id of image 2)
                // radius: array(radius blur for image 1, high pass radius for image 2)
                // opacity: array(opacity of image 1, opacity of image 2)
            case 'hybrid':
                let source1 = this.imageManager.getImage(options.imageIds[0]);
                let source2 = this.imageManager.getImage(options.imageIds[1]);

                let lsf = this.imageManipulator.blur({
                    gImage: source1,
                    radius: options.radius[0],
                });

                let hsf = this.imageManipulator.highPass({
                    gImage: source2,
                    radius: options.radius[1],
                });

                let result = this.imageManipulator.multiply({
                    gImages: [lsf, hsf],
                    opacities: [options.opacity[0], options.opacity[1]],
                    outputId: options.outputId,
                    thumbnails: [source1.getImageThumbnail(), source2.getImageThumbnail()],
                    scale: options.scale,
                });

                // If the image doesn't exist in imageManager, then assume that this is a new image
                // Save this state as default, so that it can be easily restored
                let isDefault = !this.imageManager.isImageLoaded(options.outputId);
                if (isDefault) {
                    result.pushImageData(); // Save its current state
                    this.setDefaultHybridSettings(options.outputId, options);
                }

                // Save the currently applied settings
                this.setHybridSettings(options.outputId, options);

                this.imageManager.addImage({
                    gImage: result,
                    id: options.outputId,
                });
                break;

            case 'multiply':
                images = this.imageManager.getImages(options.imageIds);

                result = this.imageManipulator.multiply({
                    gImages: images,
                    opacities: options.opacities,
                    outputId: options.outputId,
                    scale: options.scale,
                });
                result.pushImageData();

                this.imageManager.addImage({
                    gImage: result,
                    id: options.outputId,
                });
                break;

            case 'zDepth':
                // Use the ids to get the actual image object
                images = this.imageManager.getImages(options.imageIds);
                mattes = this.imageManager.getImages(options.matteIds);

                // Make a new composite image
                result = this.imageManipulator.zDepth({
                    gImages: images,
                    gMattes: mattes,
                    outputId: options.outputId,
                    scale: options.scale,
                });

                // Save the composite into the imageManager
                this.imageManager.addImage({
                    gImage: result,
                    id: options.outputId,
                })
                break;
        }
    }

    // Execute the task if all dependencies are loaded
    _dependencyLoaded(imageId) {
        let tasks = this.pendingTasks;

        for (let i = 0; i < tasks.length; i++) {
            let task = tasks[i];

            // Remove the dependency from pending dependencies
            let dependency = task.pendingDependencies[imageId];
            if (dependency) {
                delete task.pendingDependencies[imageId];
            }

            // Check if all dependencies are removed
            let length = Object.keys(task.pendingDependencies).length;
            if (length === 0) {
                delete task.pendingDependencies;
                this.pendingTasks.splice(i, 1);
                i--;
                this._beginTask(task);
            }
        }
    }

    _settingsChanged() {
        let id = this.getActiveHybrid();
        let settings = this.getHybridSettings(id);

        // Set new thumbnails
        let hybrid = this.getImage(id);
        let originalImage1 = this.getImage(settings.imageIds[0]);
        let originalImage2 = this.getImage(settings.imageIds[1]);
        let thumbnail1 = originalImage1.getImageThumbnail();
        let thumbnail2 = originalImage2.getImageThumbnail();
        hybrid.setImageThumbnails([thumbnail1, thumbnail2]);

        this.composite(settings);
        this.applyToCanvas(this.getActiveHybrid());
    }

    // Return an array of variables that are considered to be dependencies
    _getDependencies(options) {
        let dependencies = [];

        if (options.imageIds) {
            dependencies = dependencies.concat(options.imageIds);
        }

        if (options.imageId) {
            dependencies.push(options.imageId);
        }

        if (options.matteIds) {
            dependencies = dependencies.concat(options.matteIds);
        }

        return dependencies;
    }
}