class ImageManipulator {
    constructor(manager) {
        this.imageUtility = new ImageUtility();
    }

    // Image Filters With Canvas tutorial:
    // https://www.html5rocks.com/en/tutorials/canvas/imagefilters/
    // Further code needed to support alpha
    convolute(options) {
        let kernel = options.kernel;
        let gImage = options.gImage;
        let kWidth = Math.sqrt(kernel.length); // kernel width
        let kHalfWidth = Math.floor(kWidth/2);

        // Error checking can go here if we need it
        // Eg. if the kernel cannot be square rooted

        let result = this.imageUtility.createImage({
            copy: gImage,
            id: options.outputId,
            scale: options.scale,
        });

        let imageData = result.getImageData();
        let imgWidth = imageData.width;
        let imgHeight = imageData.height;
        let pixels = imageData.data;

        // Iterate over image
        for (let x = 0; x < imgWidth; x++) {
            for (let y = 0; y < imgHeight; y++) {
                let r = 0;
                let g = 0;
                let b = 0;

                // calculate weighted sum based on the kernel
                // kx is kernel's 'x' position
                for (let kx = 0; kx < kWidth; kx++) {
                    for (let ky = 0; ky < kWidth; ky++) {
                        // Position of our currently selected kernel pixel on the image
                        let posX = x + kx - kHalfWidth;
                        let posY = y + ky - kHalfWidth;
                        // Check if within bounds of the image
                        if (posX >= 0 && posX < imgWidth && posY >=0 && posY < imgHeight) {
                            let weight = kernel[ky*kWidth+kx];
                            let imgOffset = (posY*imgWidth+posX)*4;

                            // Cumulatively add the calculated values to r,g,b
                            r += pixels[imgOffset] * weight;
                            g += pixels[imgOffset+1] * weight;
                            b += pixels[imgOffset+2] * weight;
                        }
                    }
                }

                // Set the weighted sum to the pixel value
                let resultOffset = (y*imgWidth+x)*4;
                pixels[resultOffset] = r;
                pixels[resultOffset+1] = g;
                pixels[resultOffset+2] = b;
            }
        }

        // Apply the new image data
        result.applyImageData(imageData);
        return result;
    }

    blur(options = {}) {
        let img1 = options.gImage;

        let blurredImage = this._gaussianBlur({
            gImage: img1,
            radius: options.radius,
            outputId: options.outputId,
        });

        return blurredImage;
    }

    highPass(options={}) {
        let sourceImage = options.gImage;

        let blurredImage = this.blur({
            gImage: sourceImage,
            radius: options.radius,
        });

        let highPass = this.subtract({
            gImages: [sourceImage, blurredImage],
            outputId: options.outputId,
            offset: 120,
        });

        return highPass;
    }

    // Subtract image 2 from image 1
    subtract(options={}) {
        let img1 = options.gImages[0];
        let img2 = options.gImages[1];
        let imageData1 = img1.getImageData();
        let imageData2 = img2.getImageData();
        let data1 = imageData1.data;
        let data2 = imageData2.data;
        let offset = options.offset || 0;
        let includeAlpha = options.includeAlpha || false;

        let subtracted = this.imageUtility.createImage({
            copy: img1,
            id: options.outputId,
            scale: options.scale,
        });
        let subtractedImageData = subtracted.getImageData();
        let subtractedData = subtractedImageData.data;

        let width = img1.getWidth();
        let height = img1.getHeight();
        let r1,r2,g1,g2,b1,b2,a1,a2;
        for (let i=0; i < data1.length; i += 4) {
            r1 = data1[i];
            g1 = data1[i+1];
            b1 = data1[i+2];
            a1 = data1[i+3];
            r2 = data2[i];
            g2 = data2[i+1];
            b2 = data2[i+2];
            a2 = data2[i+3];

            subtractedData[i] = this._clip(r1 - r2 + offset); // Red
            subtractedData[i+1] = this._clip(g1 - g2 + offset); // Green
            subtractedData[i+2] = this._clip(b1 - b2 + offset); // Blue
            if (includeAlpha) {
                subtractedData[i+3] = this._clip(a1 - a2 + offset); // Alpha
            }
        }
        subtracted.applyImageData(subtractedImageData);

        return subtracted;
    }

    // Subtract image 2 from image 1
    multiply(options={}) {
        let img1 = options.gImages[0];
        let img2 = options.gImages[1];
        let imageData1 = img1.getImageData();
        let imageData2 = img2.getImageData();
        let data1 = imageData1.data;
        let data2 = imageData2.data;
        let opacity1 = options.opacities[0] || 1;
        let opacity2 = options.opacities[1] || 1;

        options.copy = img1;
        let multiplied = this.imageUtility.createImage(options);
        let multipliedImageData = multiplied.getImageData();
        let multipliedData = multipliedImageData.data;

        let width = img1.getWidth();
        let height = img1.getHeight();
        let r1,r2,g1,g2,b1,b2,a1,a2;
        let bright1, bright2;
        for (let i=0; i < data1.length; i += 4) {
            r1 = data1[i];
            g1 = data1[i+1];
            b1 = data1[i+2];
            a1 = data1[i+3];
            r2 = data2[i];
            g2 = data2[i+1];
            b2 = data2[i+2];
            a2 = data2[i+3];

            // When applying the opacity, increase the brightness of the colour
            // to simulate a white background
            bright1 = 255 * (1-opacity1);
            bright2 = 255 * (1-opacity2);

            multipliedData[i] = (r1 * opacity1 + bright1) * (r2 * opacity2 + bright2) / 255; // Red
            multipliedData[i+1] = (g1 * opacity1 + bright1) * (g2 * opacity2 + bright2) / 255; // Green
            multipliedData[i+2] = (b1 * opacity1 + bright1) * (b2 * opacity2 + bright2) / 255; // Blue
            multipliedData[i+3] = a1 * a2; // Alpha
        }
        multiplied.applyImageData(multipliedImageData);

        return multiplied;
    }

    zDepth(options = {}) {
        let img1 = options.gImages[0];
        let img2 = options.gImages[1];
        let matte1 = options.gMattes[0];
        let matte2 = options.gMattes[1];

        let result = this.imageUtility.createImage({
            copy: img1,
            id: options.outputId,
            scale: options.scale,
        });

        let width = img1.getWidth();
        let height = img1.getHeight();
        for (let i = 0; i < width; i++) {
            for (let j = 0; j < height; j++) {
                let m1 = matte1.getRGB(i, j);
                let m2 = matte2.getRGB(i, j);
                let b1 = (m1.red + m1.green + m1.blue) / 3;
                let b2 = (m2.red + m2.green + m2.blue) / 3;

                let s = b1 >= b2 ? img1.getRGB(i, j) : img2.getRGB(i, j);
                result.setRGB(s, i, j);
            }
        }
        result.applyImageData();

        return result;
    }

    // http://blog.ivank.net/fastest-gaussian-blur.html
    //
    // Determines values needed to get an approximate gaussian blur using box blurring at the given radius
    _boxesForGauss(sigma, n) {
        let wIdeal = Math.sqrt((12*sigma*sigma/n)+1); // Ideal averaging filter width
        let wl = Math.floor(wIdeal); // Round downwards
        if (wl%2==0) wl--; // If even, make odd
        let wu = wl+2;

        let mIdeal = (12*sigma*sigma - n*wl*wl - 4*n*wl - 3*n)/(-4*wl - 4); // seems like the max ideal width?
        let m = Math.round(mIdeal); // Round upwards

        let sizes = [];
        for(let i=0; i<n; i++) {
            // If i is less than max, use wl. Otherwise, use wu
            sizes.push(i<m?wl:wu);
        }

        return sizes;
    }

    // http://blog.ivank.net/fastest-gaussian-blur.html
    _gaussianBlur(options) {
        let boxSizes = this._boxesForGauss(options.radius, 3);
        let width = options.gImage.getWidth();
        let height = options.gImage.getHeight();
        let sourceImageData = options.gImage.getImageData();
        let sourceData = sourceImageData.data;

        let target = this.imageUtility.createImage({
            copy: options.gImage,
            id: options.outputId,
            scale: options.scale,
        });

        let targetImageData = target.getImageData();
        let targetData = targetImageData.data;

        // The solution works on a channel by channel basis
        // If we have time, we could refactor it to handle all 4 channels at once
        // This is messy
        let sourceArray = new Array(
            new Array(sourceData.length / 4),
            new Array(sourceData.length / 4),
            new Array(sourceData.length / 4),
            new Array(sourceData.length / 4),
        );
        let targetArray = new Array(
            new Array(sourceData.length / 4),
            new Array(sourceData.length / 4),
            new Array(sourceData.length / 4),
            new Array(sourceData.length / 4),
        );
        for (let i=0; i < sourceData.length/4; i++) {
            let j = i*4;
            sourceArray[0][i] = targetArray[0][i] = sourceData[j];
            sourceArray[1][i] = targetArray[1][i] = sourceData[j+1];
            sourceArray[2][i] = targetArray[2][i] = sourceData[j+2];
            sourceArray[3][i] = targetArray[3][i] = sourceData[j+3];
        }

        for (let i=0; i<sourceArray.length; i++) {
            // These three box blurs create a gaussian effect
            // Modifies targetData
            this._boxBlur(sourceArray[i], targetArray[i], width, height, (boxSizes[0]-1)/2, true);
            this._boxBlur(targetArray[i], sourceArray[i], width, height, (boxSizes[1]-1)/2, true);
            this._boxBlur(sourceArray[i], targetArray[i], width, height, (boxSizes[2]-1)/2, true);
        }

        for (let i=0; i < targetArray[0].length; i++) {
            let j = i*4;
            targetData[j] = targetArray[0][i];
            targetData[j+1] = targetArray[1][i];
            targetData[j+2] = targetArray[2][i];
            targetData[j+3] = targetArray[3][i];
        }
        target.applyImageData(targetImageData);
        return target;
    }

    // http://blog.ivank.net/fastest-gaussian-blur.html
    _boxBlur(sourceData, targetData, width, height, radius, alreadyCopied) {
        // Copy sourceData into targetData
        if (!alreadyCopied) {
            for(let i=0; i<sourceData.length; i++) {
                targetData[i] = sourceData[i];
            }
        }

        this._boxBlurH(targetData, sourceData, width, height, radius);
        this._boxBlurW(sourceData, targetData, width, height, radius);
    }

    // http://blog.ivank.net/fastest-gaussian-blur.html
    _boxBlurH(sourceData, targetData, width, height, radius) {
        let iarr = 1 / (radius + radius + 1);
        for (let i=0; i<height; i++) {
            let ti = i*width;
            let li = ti;
            let ri = ti+radius;
            let fv = sourceData[ti]; // first value
            let lv = sourceData[ti+width-1]; // last value
            let val = (radius+1)*fv;

            for(let j=0; j<radius; j++) {
                val += sourceData[ti+j];
            }

            for(let j=0; j<=radius; j++) {
                val += sourceData[ri++] - fv;
                targetData[ti++] = Math.round(val*iarr);
            }

            for(let j=radius+1; j<width-radius; j++) {
                val += sourceData[ri++] - sourceData[li++];
                targetData[ti++] = Math.round(val*iarr);
            }

            for(let j=width-radius; j<width; j++) {
                val += lv-sourceData[li++];
                targetData[ti++] = Math.round(val*iarr);
            }
        }
    }

    _boxBlurW(sourceData, targetData, width, height, radius) {
        let iarr = 1 / (radius + radius + 1);
        for (let i=0; i<width; i++) {
            let ti = i;
            let li = ti;
            let ri = ti+radius*width;
            let fv = sourceData[ti]; // first value
            let lv = sourceData[ti+height-1]; // last value
            let val = (radius+1)*fv;

            for(let j=0; j<radius; j++) {
                val += sourceData[ti+j*width];
            }

            for(let j=0; j<=radius; j++) {
                val += sourceData[ri] - fv;
                targetData[ti] = Math.round(val*iarr);
                ri += width;
                ti += width;
            }

            for(let j=radius+1; j<height-radius; j++) {
                val += sourceData[ri] - sourceData[li];
                targetData[ti] = Math.round(val*iarr);
                li += width;
                ri += width;
                ti += width;
            }

            for(let j=height-radius; j<height; j++) {
                val += lv-sourceData[li];
                targetData[ti] = Math.round(val*iarr);
                li += width;
                ti += width;
            }
        }
    }

    _clip(rgb) {
        if (rgb > 255) {
            return 255;
        }
        else if (rgb < 0) {
            return 0;
        }

        return rgb;
    }
}