class DocumentManager {
    constructor(options) {
        this.appendedByTaskNum = {};
        this.containerId = options.containerId;
    }

    getCanvasContainerId() {
        return this.containerId;
    }

    // Creates a container with a header, and appends it to the given element
    append(options = {}) {
        let container = document.createElement('div');
        let gImage = options.gImage;
        let target = options.target;
        container.setAttribute('class', "image-container")
        container.setAttribute('id', gImage.id + '-container');

        if (options.title) {
            let header = this.createHeader(options.title);
            container.appendChild(header);
        }

        let canvasContainer = document.createElement('div');
        canvasContainer.setAttribute('class', 'canvas-container');
        canvasContainer.appendChild(gImage.getCanvas());
        container.appendChild(canvasContainer);

        let targetId = target.getAttribute('id');
        let nextSibling = this._getNextSiblingId(targetId, options.taskNum);
        if (!nextSibling) {
            target.appendChild(container);
        } else {
            let siblingNode = document.getElementById(nextSibling + '-container');
            target.insertBefore(container, siblingNode);
        }

        this.appendedByTaskNum[options.taskNum] = options;
        if (gImage.scale) {
            this.setScaleTo(gImage.scale, gImage.id);
        }
    }

    applyToCanvas(canvasId, gImage, hybridSettings) {
        let canvas = document.getElementById(canvasId);
        let context = canvas.getContext('2d');

        let imageData = gImage.getImageData();
        canvas.setAttribute('width', imageData.width+'px');
        canvas.setAttribute('height', imageData.height+'px');
        context.putImageData(imageData, 0, 0);

        // Also apply thumbnails
        let thumbnail1 = document.getElementById('hybrid-thumbnail-1');
        let thumbnail2 = document.getElementById('hybrid-thumbnail-2');
        thumbnail1.setAttribute('src', gImage.getImageThumbnail(0));
        thumbnail2.setAttribute('src', gImage.getImageThumbnail(1));

        // Set control panel values
        let scaleSlider = document.getElementById("image-scale-slider");
        scaleSlider.value = hybridSettings.scale * 100;
        this.setScaleTo(hybridSettings.scale, this.containerId);

        // Blur for low spatial frequency
        let lsfBlurSlider = document.getElementById("lsf-blur-slider");
        lsfBlurSlider.value = hybridSettings.radius[0];

        // Blur for high spatial frequency
        let hsfBlurSlider = document.getElementById("hsf-blur-slider");
        hsfBlurSlider.value = hybridSettings.radius[1];

        // Opacity for low spatial frequency
        let lsfOpacitySlider = document.getElementById("lsf-opacity-slider");
        lsfOpacitySlider.value = hybridSettings.opacity[0] * 100;

        // Opacity for low spatial frequency
        let hsfOpacitySlider = document.getElementById("hsf-opacity-slider");
        hsfOpacitySlider.value = hybridSettings.opacity[1] * 100;

        this.updateSliderNumbers();
    }

    createDots(num) {
        let dotContainer = document.getElementById('dot-container');
        for(let i=0; i < num; i++) {
            let dot = document.createElement('span');
            dot.setAttribute('class', 'dot');
            dotContainer.append(dot);
        }
    }

    createHeader(title) {
        let header = document.createElement('h4');
        header.setAttribute('class', 'image-header');
        header.textContent = title;

        return header;
    }

    // Set active dot
    setActiveDot(num = 0) {
        let dotContainer = document.getElementById('dot-container');
        let dots = dotContainer.getElementsByClassName('dot');
        for (let i=0; i < dots.length; i++) {
            let dot = dots[i];
            let dotClass = dot.getAttribute('class');
            if (dotClass.includes('active')) dot.setAttribute('class', 'dot');
            if (num === i) dot.setAttribute('class', 'dot active');
        }
    }

    // Not cumulative, and doesn't support multiple ids
    // Additional work needs to be done if it's needed
    setScaleTo(scale, containerId) {
        let container = document.getElementById(containerId);
        if (container) {
            let canvas = container.getElementsByTagName('canvas')[0];

            let style = window.getComputedStyle(canvas);
            let width = parseInt(style.width);
            let height = parseInt(style.height);

            container.setAttribute('style',
                `width: ${width * scale}px;
                 height: ${height * scale}px`);
            canvas.setAttribute('style',
                `transform: scale(${scale});`);
        }
    }

    updateSliderNumbers() {
        this.placeNumberOverSlider('scale-container');
        this.placeNumberOverSlider('lsf-blur-container');
        this.placeNumberOverSlider('hsf-blur-container');
        this.placeNumberOverSlider('lsf-opacity-container');
        this.placeNumberOverSlider('hsf-opacity-container');
    }

    placeNumberOverSlider(containerId) {
        let container = document.getElementById(containerId);
        let slider = container.getElementsByTagName('input')[0];
        let number = container.getElementsByClassName('slider-number-value')[0];
        number.textContent = slider.value;
        let sliderRect = slider.getBoundingClientRect();
        let numberRect = number.getBoundingClientRect();

        let top = sliderRect.top - sliderRect.height/2 - numberRect.height/2;
        let ratio = slider.value / slider.max;
        // This is dependent on the css
        // Hardcoded numbers were found through trial and error, in the interest of time.
        let left = sliderRect.left + (sliderRect.width - 21) * ratio - numberRect.width/2;
        number.style.setProperty('left', left + 'px');
        number.style.setProperty('top', top + 'px');
    }

    _getNextSiblingId(targetId, taskNum) {
        let greaterThan = "";
        let lowerThan = "";
        let siblings = this.appendedByTaskNum;

        if (siblings) {
            let keys = Object.keys(siblings);
            for(let i = 0; i < keys.length; i++) {
                let key = keys[i];
                let sibling = siblings[key];
                let siblingTargetId = sibling.target.getAttribute('id');

                if (taskNum < sibling.taskNum) {
                    if (siblingTargetId === targetId) {
                        return sibling.gImage.id;
                    }
                }
            }
        }

        return false;
    }
}